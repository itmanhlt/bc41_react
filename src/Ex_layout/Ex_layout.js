import React, { Component } from "react";
import Header from "./Header";
import Navigation from "./Navigation";
import Content from "./Content";
import Footer from "./Footer";

export default class Ex_layout extends Component {
  render() {
    return (
      <div class="row">
        Ex_layout
        <Header />
        <Navigation />
        <Content />
        <Footer />
      </div>
    );
  }
}
