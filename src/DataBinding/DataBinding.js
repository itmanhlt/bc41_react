import React, { Component } from "react";

export default class DataBinding extends Component {
  username = "Alice";
  render() {
    return (
      <div>
        <nav className="nav">
          <a className="nav-link active" href="#">
            Active
          </a>
          <a className="nav-link" href="#">
            Link
          </a>
          <a className="nav-link" href="#">
            Link
          </a>
          <a
            className="nav-link disabled"
            href="#"
            tabIndex={-1}
            aria-disabled="true"
          >
            Disabled
          </a>
        </nav>

        <h2>DataBinding</h2>
        <h4>Welcome to {this.username}</h4>
      </div>
    );
  }
}
