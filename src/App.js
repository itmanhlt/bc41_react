// import logo from "./logo.svg";
import "./App.css";
import Ex_Demo_Redux_Mini from "./Ex_Demo_Redux_Mini/Ex_Demo_Redux_Mini";
import Ex_Phone from "./Ex_Phone/Ex_Phone";
import Ex_Shoe_Shop_Redux from "./Ex_Shoe_Shop_Redux/Ex_Shoe_Shop_Redux";
// import Ex_Car_Color from "./Ex_Car_Color/Ex_Car_Color";
// import DemoProps from "./DemoProps/DemoProps";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DemoState from "./DemoState/DemoState";
// import DemoClass from "./DemoComponent/DemoClass";
// import DemoFunction from "./DemoComponent/DemoFunction";
// import Ex_layout from "./Ex_layout/Ex_layout";
// import DataBinding from "./DataBinding/DataBinding";
// import EventHandling from "./EventHandling/EventHandling";
// impoRenderWithMap
import Ex_Shoe_Shop from "./Ex_Shoe_Shop/Ex_Shoe_Shop";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction /> */}
      {/* <Ex_layout /> */}
      {/* <DataBinding /> */}
      {/* <EventHandling /> */}
      {/* <ConditionalRender /> */}
      {/* <DemoState /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_Car_Color /> */}
      {/* <DemoProps /> */}
      {/* <Ex_Shoe_Shop /> */}
      {/* <Ex_Phone /> */}
      {/* <Ex_Demo_Redux_Mini /> */}
      <Ex_Shoe_Shop_Redux />
    </div>
  );
}

export default App;
