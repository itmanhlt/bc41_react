import React, { Component } from "react";

export default class EventHandling extends Component {
  handleLogin = () => {
    console.log("yes");
  };
  handleSayHelloWithName = (username) => {
    console.log("hello", username);
  };
  render() {
    return (
      <div>
        <h2>EventHandling</h2>
        <button
          onClick={() => {
            this.handleSayHelloWithName("Alice");
          }}
          className="btn btn-success"
        >
          Login
        </button>
      </div>
    );
  }
}
