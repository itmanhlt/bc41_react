import React, { Component } from "react";
import Cart from "./Cart";
import { data_shoe } from "./DataShoe";
import ListShoe from "./ListShoe";

export default class Ex_Shoe_Shop_Redux extends Component {
  render() {
    return (
      <div>
        <h2>Ex_Shoe_Shop</h2>
        <div className="row">
          <div className="col-6">
            <Cart />
          </div>
          <div className="col-6">
            <ListShoe />
          </div>
        </div>
      </div>
    );
  }
}
