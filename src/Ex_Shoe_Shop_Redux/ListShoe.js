import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";

class ListShoe extends Component {
  render() {
    return (
      <div className="container">
        <h2>ListShoe</h2>
        <div className="row">
          {this.props.list.map((item, index) => {
            return <ItemShoe key={index} shoe={item} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.list,
  };
};

export default connect(mapStateToProps)(ListShoe);
