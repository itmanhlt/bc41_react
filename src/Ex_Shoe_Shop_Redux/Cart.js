import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  DELETE_TO_CART,
} from "./reducer/constant/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => this.props.handleQuantity(item.id, -1)}
              className="btn btn-danger"
            >
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => this.props.handleQuantity(item.id, 1)}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => this.props.handleDelete(item.id)}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>price</th>
              <th>quantity</th>
              <th>image</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (idShoe) => {
      dispatch({
        type: DELETE_TO_CART,
        payload: idShoe,
      });
    },
    handleQuantity: (id, soLuong) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { id, soLuong },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
