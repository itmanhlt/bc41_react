import { data_shoe } from "../DataShoe";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  DELETE_TO_CART,
} from "./constant/shoeConstant";

let initialValue = {
  list: data_shoe,
  cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE_TO_CART: {
      let newCart = state.cart.filter((item) => {
        return item.id != action.payload;
      });
      console.log("DELETE_TO_CART", newCart);
      return { ...state, cart: newCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (action.payload.soLuong == 1) {
        cloneCart[index].soLuong += action.payload.soLuong;
      } else {
        cloneCart[index].soLuong += action.payload.soLuong;
      }
      console.log("CHANGE_QUANTITY", cloneCart[index].soLuong);
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
