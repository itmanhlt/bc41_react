import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./reducer/constant/shoeConstant";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handlePushToCart(this.props.shoe);
            }}
            href="#"
            className="btn btn-primary"
          >
            add to card
          </a>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePushToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
