import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  state = {
    userName: "Alice",
    age: 10,
  };
  hanndleChangeUserName = () => {
    let name;
    if (this.state.userName == "Alice") {
      name = "Bob";
    } else {
      name = "Alice";
    }
    this.setState({ userName: name });
  };
  render() {
    return (
      <div>
        <h2>DemoProps</h2>
        <UserInfo
          hoTen={this.state.userName}
          tuoi={this.state.age}
          handleOnclick={this.hanndleChangeUserName}
        />
      </div>
    );
  }
}
