import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    return (
      <div>
        <h2>UserInfo</h2>
        <h3>UserName: {this.props.hoTen}</h3>
        <h3>Age: {this.props.tuoi}</h3>
        <button onClick={this.props.handleOnclick} className="btn btn-success">
          Change UserName
        </button>
      </div>
    );
  }
}
