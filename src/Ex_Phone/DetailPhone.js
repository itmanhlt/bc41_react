import React, { Component } from "react";

export default class DetailPhone extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      CameraTruoc,
      CameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="row">
        <div className="col-4">
          <img src={hinhAnh} alt="" />
        </div>
        <div className="col-8">
          <h2>{maSP}</h2>
          <h2>{tenSP}</h2>
          <h2>{manHinh}</h2>
          <h2>{heDieuHanh}</h2>
          <h2>{CameraTruoc}</h2>
          <h2>{CameraSau}</h2>
          <h2>{ram}</h2>
          <h2>{rom}</h2>
          <h2>{giaBan}</h2>
        </div>
      </div>
    );
  }
}
