import React, { Component } from "react";
import DetailPhone from "./DetailPhone";
import ListPhone from "./ListPhone";

import { data_phone } from "./data_phone";

export default class Ex_Phone extends Component {
  state = {
    listPhone: data_phone,
    detailPhone: data_phone[0],
  };

  render() {
    return (
      <div>
        <h2>Ex_Phone</h2>
        <ListPhone list={this.state.listPhone} />
        <DetailPhone detail={this.state.detailPhone} />
      </div>
    );
  }
}
