let initialValue = {
  soLuong: 100,
};
//initialValue gia tri mac dinh cua state khi lan dau load trang

export const numberReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      console.log("Xu ly tang so luong");
      state.soLuong++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      console.log("Xu ly giam so luong");
      state.soLuong -= state.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
