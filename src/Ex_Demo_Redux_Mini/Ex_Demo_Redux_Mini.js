import React, { Component } from "react";
import { connect } from "react-redux";

class Ex_Demo_Redux_Mini extends Component {
  render() {
    console.log("props", this.props);
    return (
      <div>
        <h2>Ex_Demo_Redux_Mini</h2>
        <button
          onClick={this.props.handleGiamSoluong}
          className="btn btn-danger"
        >
          -
        </button>
        <strong className="mx-2">{this.props.soLuong}</strong>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.soLuong,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoluong: () => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: 10,
      };
      dispatch(action);
    },
  };
};

//key: ten props (cua component)
//value: gia tri muon lay tu state (cua redux)
export default connect(mapStateToProps, mapDispatchToProps)(Ex_Demo_Redux_Mini);
