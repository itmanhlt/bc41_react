import React, { Component } from "react";

export default class ConditionalRender extends Component {
  isLogin = false;
  handleLogin = () => {
    console.log("Before Login", this.isLogin);
    this.isLogin = true;
    console.log("After Login", this.isLogin);
  };
  handleLogout = () => {
    console.log("Before Login", this.isLogin);
    this.isLogin = false;
    console.log("After Login", this.isLogin);
  };
  renderContentButton = () => {
    if (this.isLogin == true) {
      return (
        <button onClick={this.handleLogout} className="btn btn-btn-danger">
          Đăng xuất
        </button>
      );
    } else {
      return (
        <button onClick={this.handleLogin} className="btn btn-primary">
          Đăng nhập
        </button>
      );
    }
  };
  render() {
    return (
      <div>
        <h2>ConditionalRendering</h2>
        <div>{this.renderContentButton()}</div>
      </div>
    );
  }
}
