import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => this.props.handleQuantity(item.id, -1)}
              className="btn btn-danger"
            >
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => this.props.handleQuantity(item.id, 1)}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => this.props.handleDelete(item.id)}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>price</th>
              <th>quantity</th>
              <th>image</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
