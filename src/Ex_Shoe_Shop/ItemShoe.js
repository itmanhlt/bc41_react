import React, { Component } from "react";

export default class extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleAddToCart(this.props.shoe);
            }}
            href="#"
            className="btn btn-primary"
          >
            add to card
          </a>
        </div>
      </div>
    );
  }
}
