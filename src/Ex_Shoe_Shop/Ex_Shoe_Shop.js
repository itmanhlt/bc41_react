import React, { Component } from "react";
import Cart from "./Cart";
import { data_shoe } from "./DataShoe";
import ListShoe from "./ListShoe";

export default class Ex_Shoe_Shop extends Component {
  state = {
    listShoe: data_shoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = this.state.cart;
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (idShoe) => {
    let newCart = this.state.cart.filter((item) => {
      return item.id != idShoe;
    });
    this.setState({ cart: newCart });
  };
  handleQuantity = (id, soLuong) => {
    let cloneCart = this.state.cart;
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    if (soLuong == 1) {
      cloneCart[index].soLuong += soLuong;
    } else {
      cloneCart[index].soLuong += soLuong;
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <h2>Ex_Shoe_Shop</h2>
        <div className="row">
          <div className="col-6">
            <Cart
              handleQuantity={this.handleQuantity}
              cart={this.state.cart}
              handleDelete={this.handleDelete}
            />
          </div>
          <div className="col-6">
            <ListShoe
              handleAdd={this.handleAddToCart}
              list={this.state.listShoe}
            />
          </div>
        </div>
      </div>
    );
  }
}
