import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    userName: "Alice",
  };
  handleChangeUserName = () => {
    let name;
    if (this.state.userName == "Bob") {
      name = "Alice";
    } else {
      name = "Bob";
    }
    this.setState({ userName: name });
  };
  render() {
    return (
      <div>
        <h2>DemoState</h2>
        <h3
          className={
            this.state.userName == "Bob" ? "text-danger" : "text-primary"
          }
        >
          {this.state.userName}
        </h3>
        <button onClick={this.handleChangeUserName} className="btn btn-primary">
          Change UserName
        </button>
      </div>
    );
  }
}
